//
//  ButtonView.swift
//  HanbergerMenuSample
//
//  Created by Yamada Masaaki on 2020/07/04.
//  Copyright © 2020 Yamada Masaaki. All rights reserved.
//

import SwiftUI

struct ButtonView: View {
    var body: some View {
        HStack{
            Image(systemName: "square.and.arrow.up")
            Image(systemName: "pencil")
            Image(systemName: "trash")
        }
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView()
    }
}
