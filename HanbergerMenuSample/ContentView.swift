//
//  ContentView.swift
//  HanbergerMenuSample
//
//  Created by Yamada Masaaki on 2020/07/04.
//  Copyright © 2020 Yamada Masaaki. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    // フラグ（Ver1.0.1変更）
    @State var flg1 = false
    
    var body: some View {
        HStack {
            Spacer()
            if flg1 {
                ButtonView()
                    .transition(AnyTransition.opacity.combined(with: .offset(x: 100, y: 0)))
            }
            VStack{
                Button(action: {
                    withAnimation() {
                        self.flg1.toggle()
                        print(self.flg1.description)
                    }
                }) {
                    Image(systemName: "list.number")
               }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
